<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** to Handel 404 */
Route::fallback(function(){
    return response()->json(['status' => 404,'message' => 'Not Found.'], 200);
});



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([

    'middleware' => 'api',
    'prefix' => 'auth/user'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('forget_password', 'AuthController@forget_password');
    Route::post('reset_password', 'AuthController@reset_password');
        
});

Route::group(['middleware' => ['jwt.verify', 'admin'], 'prefix' => 'admin'], function(){

    /** Add & Edit User */
    Route::post('user/create', 'AuthController@create');
    Route::post('user/update', 'AuthController@update');
    
    /** This Section To Add and Update Projects */
    Route::post('project/create', 'adminController@add_project');
    Route::post('project/update', 'adminController@update_project');

    /** This Section To Add and Update Zones */
    Route::post('zones/create', 'adminController@add_zone');
    Route::post('zones/update', 'adminController@update_zone');

    /** This Section To Add and Update Types and Sub_types */
    Route::post('type/create', 'adminController@add_type');
    Route::post('type/update', 'adminController@update_type');

});


Route::group(['middleware' => ['jwt.verify', 'user.active', 'auth:api']], function() {

    Route::post('update_password', 'AuthController@update_password');
    Route::post('logout', 'AuthController@logout');
    
});


Route::group(['middleware' => ['jwt.verify', 'user.active'] , 'prefix' => 'user'], function() {
    /** Get All User Projects */
    Route::get('projects', 'userController@projects');


    /** Report Section [create - report details - report list] */
    Route::post('report/create', 'userController@add_report');
    Route::get('report/{report_hash}', 'userController@report_details');
    Route::get('reports', 'userController@reports_list');

    /** Add Note To Report */
    Route::post('report/note/create', 'userController@add_note');
    
    /** Dashboard Data */
    Route::get('dashboard', 'userController@dashboard_data');

    /** User Notifications */
    Route::get('all_notifications', 'userController@all_notifications');

    /** Zone Images */
    Route::get('get_zone_images', 'userController@get_zone_images');
});

Route::post('file/upload', 'userController@upload_files');
