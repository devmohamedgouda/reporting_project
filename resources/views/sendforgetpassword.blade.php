@component('mail::message')

Hello {{$data['user_name']}},<br>

Follow this link to reset your password for your {{$data['user_email']}} account.<br>

<a href="{{$data['link']}}">{{$data['link']}} </a><br>

or use this code as a reset code: {{$data['code']}}<br>

If you didn’t ask to reset your password, you can ignore this email.<br>

Thanks,

