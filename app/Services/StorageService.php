<?php

namespace App\Services;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class StorageService {

    public function getFolderImages($project, $zone, $date)
    {
        $date = date("Y_m_d", strtotime($date));
        $files = Storage::disk('s3')->files("{$project}/{$zone}/{$date}");

        $signed_files = [];

        foreach ($files as $file) {
            $signed_files[] = [
                "name" => basename($file),
                "path" => $this->getSignedUri($file)
            ];
        }

        return $signed_files;
    }

    public function getSignedUri($file)
    {
        $s3 = Storage::disk('s3');
        $client = $s3->getDriver()->getAdapter()->getClient();
        $expiry = "+60 minutes";
        $command = $client->getCommand('GetObject', [
            'Bucket' => Config::get('filesystems.disks.s3.bucket'),
            'Key'    => $file
        ]);
        $request = $client->createPresignedRequest($command, $expiry);

        return (string)$request->getUri();
    }
}