<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class report extends Model
{ 


    protected $fillable = [
        'report_hash', 'project_id', 'zone_id', 'user_id', 'report_time', 'is_critical', 'comment',
         'action', 'main_image', 'is_report'
    ];
    
    public function zone()
    {
        return $this->belongsTo(zone::class);
    }

    public function project()
    {
        return $this->belongsTo(project::class);
    }

    public function type()
    {
        return $this->hasMany('App\report_type', 'report_id', 'id');
    }

    public function report_imgs(){
        return $this->hasMany('App\report_image', 'report_id', 'id');
    }

    public function report_notes(){
        return $this->hasMany('App\report_note', 'report_id', 'id')->with('user_data:id,name,email');
    }

    public function project_parent(){
        return $this->belongsTo('App\project', 'project_id', 'id');
    }



    /** define an accessors to custem created_at and updated_at */
    public function getcreatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i a');
    }

    public function getupdatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['updated_at'])->format('Y-m-d g:i a');
    }

    public function getReportTimeAttribute($value)
    {
       $date = Carbon::parse($this->attributes['report_time'])->format('Y-m-d g:i a');
       if($date){
           return $date;
       }else{
           return 'error';
       }
    }
  
 }
