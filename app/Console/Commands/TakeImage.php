<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class TakeImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'camera:take_image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // get image content from camera 1
        $file_content = file_get_contents("http://admin:password@105.199.23.18:1050/dms?nowprofileid=1");
        $file_name = date("Y_m_d_h_i");
        $folder = date("Y_m_d");
        Storage::disk('s3')->put("project1/zone1/{$folder}/{$file_name}.jpg", $file_content);

        $s3 = Storage::disk('s3');
        $client = $s3->getDriver()->getAdapter()->getClient();
        $expiry = "+10 minutes";
        $command = $client->getCommand('GetObject', [
            'Bucket' => Config::get('filesystems.disks.s3.bucket'),
            'Key'    => "project1/zone1/{$folder}/{$file_name}.jpg"
        ]);
        $request = $client->createPresignedRequest($command, $expiry);

        echo (string)$request->getUri();

        $file_content = file_get_contents("http://admin:password@105.199.23.18:1060/dms?nowprofileid=1");

        Storage::disk('s3')->put("project1/zone1/{$folder}/{$file_name}.jpg", $file_content);
    }
}
