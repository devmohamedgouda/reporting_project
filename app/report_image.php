<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class report_image extends Model
{
    protected $fillable = [
        'report_id', 'url_image'
    ];
    
  
    /** define an accessors to custem created_at and updated_at */
    public function getcreatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i a');
    }

    public function getupdatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['updated_at'])->format('Y-m-d g:i a');
    }
}
