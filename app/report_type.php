<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class report_type extends Model
{
    protected $fillable = [
        'report_id', 'type_id'
    ];

    public function type(){
        return $this->belongsTo(type::class);
    }

    public function report(){
        return $this->belongsTo(report::class);
    }


    /** define an accessors to custem created_at and updated_at */
    public function getcreatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i a');
    }

    public function getupdatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['updated_at'])->format('Y-m-d g:i a');
    }
}
