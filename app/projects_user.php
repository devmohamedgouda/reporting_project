<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class projects_user extends Model
{
    protected $fillable = [
        'user_id', 'project_id'
    ];

    public function project_data(){
        return $this->hasOne('App\project', 'id','project_id')->with('project_zones:id,name,code,address,type,project_id');
    }

    public function zones_data(){
        return $this->hasMany('App\zone', 'id', 'project_id');
    }

    public function user_data(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }


    /** define an accessors to custem created_at and updated_at */
    public function getcreatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i a');
    }

    public function getupdatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['updated_at'])->format('Y-m-d g:i a');
    }

}

