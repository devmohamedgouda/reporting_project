<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class repositoryServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Http\Interfaces\adminRepositoryInterface',
            'App\Http\Repositories\adminRepository'
        );

        $this->app->bind(
            'App\Http\Interfaces\userRepositoryInterface',
            'App\Http\Repositories\userRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
