<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;

class CustomDbChannel 
{

  public function send($notifiable, Notification $notification)
  {
    $data = $notification->toDatabase($notifiable);

    return $notifiable->routeNotificationFor('database')->create([
        'id' => $notification->id,

        //customize here
        'report_hash' => $data['report_hash'],
        'notification_type' => $data['type'],
        'date'=> $data['date'],

        'type' => get_class($notification),
        'data' => $data,
        'read_at' => null,
    ]);
  }

}