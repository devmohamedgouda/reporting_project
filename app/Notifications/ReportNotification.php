<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReportNotification extends Notification
{
    use Queueable;

    protected $arr;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $arr)
    {
        $this->arr = $arr;

    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [CustomDbChannel::class, 'mail'];
    }

    public function toDatabase(){
        return[
            'title' => $this->arr['title'],
            'date'  => $this->arr['date'],
            'type'  => $this->arr['notification_type'],
            'report_hash'  => $this->arr['report_hash'],
        ]; 
    }


    public function toMail($notifiable)
    {
        $url = env('ADMIN_URL').'api/user/report/'. $this->arr['report_hash'];

        return (new MailMessage)
            ->greeting('Hello!')
            ->line($this->arr['title']) 
            ->action('View Report' , $url)
            ->line('at: ' .$this->arr['date']);
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
