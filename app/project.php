<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class project extends Model
{
    protected $fillable = [
        'name', 'description', 'lat', 'long', 'active'
    ];
    
    
    public function project_zones(){
        return $this->hasMany('App\zone', 'project_id', 'id')->with('zone_reports_with_type:id,zone_id');
    }

    public function reports_project(){
        return $this->hasMany('App\report', 'project_id', 'id');
    }


    /** define an accessors to custem created_at and updated_at */
    public function getcreatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i a');
    }

    public function getupdatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['updated_at'])->format('Y-m-d g:i a');
    }

}
