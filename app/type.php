<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class type extends Model
{
    protected $fillable = [
        'name', 'parent'
    ];
    protected $dateFormat = 'U';

    public function type_reports(){
        return $this->hasMany('App\report_type', 'type_id')->with('report');
    }

    // public function type_reports(){
    //     return $this->hasManyThrough(report_type::class, report::class);
    // }

    /** define an accessors to custem created_at and updated_at */
    public function getcreatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i a');
    }

    public function getupdatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['updated_at'])->format('Y-m-d g:i a');
    }
}
