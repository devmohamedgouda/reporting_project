<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class zone extends Model
{
    protected $fillable = [
        'name', 'code', 'address', 'type', 'project_id', 'imgs_path'
    ];

//    protected $dateFormat = 'Y-m-d H:i:s';


    public function zone_reports(){
        return $this->hasMany('App\report', 'zone_id', 'id')->with(['report_imgs:report_id,url_image', 'report_notes:report_id,user_id,comment', 'type']);
    }

    public function zone_reports_with_type(){
        return $this->hasMany('App\report', 'zone_id', 'id')->with('type');
    }

    

    /** define an accessors to custem created_at and updated_at */
    public function getcreatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i a');
    }

    public function getupdatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['updated_at'])->format('Y-m-d g:i a');
    }
}
