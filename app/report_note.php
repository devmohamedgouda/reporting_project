<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class report_note extends Model
{
    protected $fillable = [
        'report_id','user_id', 'comment'
    ];

    public function user_data(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /** define an accessors to custem created_at and updated_at */
    public function getcreatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['created_at'])->format('Y-m-d g:i a');
    }

    public function getupdatedAtAttribute($value)
    {
       return Carbon::parse($this->attributes['updated_at'])->format('Y-m-d g:i a');
    }
}
