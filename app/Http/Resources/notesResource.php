<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class notesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id'       => $this->user_data->id,
            'user_name'       => $this->user_data->name,
            'comment'       => $this->comment,
            'date'       => $this->created_at,
        ];
    }
}
