<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class reportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'report_hash'       => $this->report_hash,
            'project_id'        => $this->project_id,
            'zone_id'           => $this->zone_id,
            'report_time'       => $this->report_time,
            'is_critical'       => $this->is_critical,
            'is_report'         => $this->is_report,
            'comment_text'      => $this->comment,
            'action_text'       => $this->action,
            'types'             => $this->types,
            'report_notes'      => notesResource::collection($this->report_notes),
            'report_imgs'       => $this->report_images,
           
        ];
    }
}
