<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationsResourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        if(is_null($this->read_at)){
            $unread = false;
        }else{
            $unread = true;
        }

        return [
            'message'       => $this->data['title'],
            'create_date'   => $this->data['date'],
            'type'          => $this->data['type'],
            'unread'        => $unread,
            'report_id'     => $this->data['report_id'],
        ];
    }
}
