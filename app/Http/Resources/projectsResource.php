<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class projectsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            
            'id'       => $this->project_data->id,
            'name'       => $this->project_data->name,
            'description'       => $this->project_data->description,
            'lat'       => $this->project_data->lat,
            'long'       => $this->project_data->long,
            'zones'    =>$this->zones_data,
            'assigned_people'    =>userResorce::collection($this->users),

        ];
    }
}
