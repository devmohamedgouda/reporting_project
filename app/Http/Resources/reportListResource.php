<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class reportListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->project_data
        ];
    }
}
