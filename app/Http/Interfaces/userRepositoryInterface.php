<?php

namespace App\Http\Interfaces;


interface userRepositoryInterface{

    /** User Projects */
    public function projects();

    /** uploder Function */
    public function upload_files($request);

    /** Add Report */
    public function add_report($request);

    /** Report Details */
    public function report_details($report_hash);

    /** Reort List */
    public function reports_list($request);

    /** Add Report Note */
    public function add_note($request);

    /** Dashboard Data */
    public function dashboard_data($request);

    /** All Notifcations */
    public function all_notifications($request);

    /** Get Zone Images */
    public function get_zone_images($request);

    
}