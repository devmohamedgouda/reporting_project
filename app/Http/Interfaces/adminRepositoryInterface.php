<?php

namespace App\Http\Interfaces;


interface adminRepositoryInterface{

    /** This Section To Add and Update Projects */
    /** Add Project */
    public function add_project($request);

    /** Update Project */
    public function update_project($request);



    /** This Section To Add and Update Zones */
    /** Add Zone */
    public function add_zone($request);

    /** Update Zone */
    public function update_zone($request);


    /** This Section To Add and Update Types and Sub_types */
    /** Add type */
    public function add_type($request);

    /** Update type */
    public function update_type($request);
}