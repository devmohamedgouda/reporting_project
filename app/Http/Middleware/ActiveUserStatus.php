<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\api\Traits\tokenTrait;
use App\Http\Controllers\api\Traits\ApiResponseTrait;

class ActiveUserStatus
{
    use tokenTrait;
    use ApiResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $user_data = $this->getAuthenticatedUser();
        if($user_data->active == 1){
            return $next($request);
        }else{
            return $this->apiResponse(400 , "This User is not active" );
        }

    }
}
