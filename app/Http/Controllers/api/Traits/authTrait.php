<?php
namespace App\Http\Controllers\api\Traits;

use App\User;
use App\projects_user;


trait authTrait{

    // Add Projects Users 
    public function add_projects_users($data, $user_id){
        $new_projects_ids = [];
        foreach ($data as $value) {
            $check = $this->search_if_user_has_exist_project($user_id, $value);
            if($check == true){
                $user_projects = projects_user::create([
                    'user_id' => $user_id,
                    'project_id' => $value,
                ]);
            }
            $new_projects_ids[] = $value;
        }
        $delete_old_preojects =projects_user::where('user_id', $user_id)->whereNotIn('project_id', $new_projects_ids)->delete();
    }

    /** Search if the user has the same project before or not */
    public function search_if_user_has_exist_project($user_id, $project_id){
        $data = projects_user::where([ ['user_id', $user_id], ['project_id', $project_id] ])->count();
        if($data > 0){
            return false;
        }else{
            return true;
        }
    }

    /** Function To Return The User Status As Active Or Not */
    public function user_active_status($email){
        $user = User::where([ ['email', $email], ['active', 1] ])->count();
        if($user > 0){
            return true;
        }else{
            return false;
        }
    }

    public function email_exist($email){
        $user = User::where('email', $email)->count();
        if($user > 0){
            return true;
        }else{
            return false;
        }
    }

}

?>