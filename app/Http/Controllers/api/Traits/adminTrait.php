<?php
namespace App\Http\Controllers\api\Traits;

use App\Notifications\ReportNotification;


trait adminTrait{

    public function get_types_and_sub_types($type_id){
         /**
         * Build:[
         *  Select Type Data at var: $type_data.
         *  Select Sub Types Data of Type at var: $sub_type_data.
         *  add $sub_type_data to $type_data with key: sub_types
         * ]
         */
        $type_data = $this->type_model::select('id', 'name')->find($type_id);
        $sub_type_data = $this->type_model::where('parent', $type_data->id)->select('id', 'name')->get();
        $type_data['sub_types'] = $sub_type_data;
        return $type_data;
    }

    public function get_types_data(){
         /**
         * Build:[
         *  Select Type Data at var: $report_types.
         *  Select Sub Types Data of Type at var: types with function get_types_and_sub_types.
         *  collect all data at types_data array.
         * ]
         */
        $types_data = [];
        $report_types = $this->type_model::where([ ['parent', null], ['active', 1] ])->get();
        foreach ($report_types as $type) {
            $types = $this->get_types_and_sub_types($type->id);
            $types_data[] = $types;
        }
        return $types_data;
        
    }

    public function add_sub_types_of_type($sub_type_data, $type_id){
         /**
         * Build:[
         *  loop of $sub_type_data and save child of it into database.
         * ]
         */
        foreach ($sub_type_data  as $value) {
            $sub_type = $this->type_model::create([
                'name' => $value,
                'parent' => $type_id,
            ]);
        }
    }

    public function add_type($type, $report_id)
    {
        $sub_type = $this->report_type_model::create([
            'type_id' => $type,
            'report_id' => $report_id,
        ]);
    
    }

    /** This Function To update Sub Types Of Type */
    public function update_sub_type($sub_type_data, $type_id){
        /**
         * Build:[
         *  loop of $sub_type_data and expload the child of it to array with ","
         *  update with array that exploaded $data[1,txt,0], {0 => id / txt => name / 0 => active}
         * ]
         */
        foreach ($sub_type_data as $sub_type) {

            $data = explode(',' , $sub_type);

            if($data[0] == ""){
                $update = $this->type_model::create([
                    'name' => $data[1],
                    'parent' => $type_id,
                ]);

            }else{

                $update = $this->type_model::find($data[0]);
                if($update){
                    $update->update([
                    'name' => $data[1],
                    'active' => $data[2]
                    ]);
                }else{
                return $this->apiResponse(422, "Validation Errors", "id is not exist");

                }
            }
        }
    }

    /** Check If user hase Access at report Or Not */
    public function user_has_access($user_id, $project_id){
        /**
         * Build:[
         *  check if user id and report id is exist at projects_user table or not
         * ]
         */
        
        $check = $this->projects_user_model::where([ ['user_id', $user_id], ['project_id', $project_id] ])->count();
        if($check > 0){
            return true;
        }else{
            return false;
        }
    }

    public function get_project_id($report_id){
          /**
         * Build:[
         *  Get Project ID from report table.
         * ]
         */
        $get_project_id = $this->report_model::where('id', $report_id)->with('project_parent:id')->first();
        return $get_project_id->project_parent->id;
    }

    /** Check If Project Active Or not */
    public function project_status($project_id){
        $project = $this->project_model::select('active')->find($project_id);
        if($project->active == 1){
            return true;
        }else{
            return false;
        }
    }

    /** Notifications Section */
    /** Send Notification To Group Of Users */
    public function notifay_users($project_id, $data, $exception_user_id){
        /**
         * Build:[
         *  Select Users That Have Accsess.
         *  Send Notification for each user.
         * ]
         */
        $users = $this->projects_user_model::where([['project_id', $project_id], ['user_id', '!=', $exception_user_id]])->select('user_id')->get();
        foreach ($users as $user) {
            $notification_user = $this->users_model::where('id', $user->user_id)->first();
            $notification = $notification_user->notify(new ReportNotification($data));
        }
    }

    /** Mark As Read Notification */
    public function mark_as_read_notification($notifications){
        /**
         * Build:[
         *  loop at notifications.
         *  mark each one as readed.
         * ]
         */
        foreach ($notifications as $row) {
            $row->markAsRead();
        }
    }


}

?>