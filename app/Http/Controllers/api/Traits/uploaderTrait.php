<?php
namespace App\Http\Controllers\api\Traits;

use App\report_image;

trait uploaderTrait{


    public function upload_multi_files($files){
       /**
         * Build:[
         *  loop at files.
         *  move file at local storage.
         *  save image at database with save_image_database function.
         * ]
         */ 
        foreach ($files as  $file) {

            $filename  = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $fileName  = time().'_image.'.$extension;
            $file->move(public_path("/images") , $fileName);

            $files_names[] =  env('ADMIN_URL')."/images/".$fileName;
        }
        return $files_names;
    }

    public function save_image_database($report_id, $image_url){
        /**
         * Build:[
         *  save image at database.
         * ]
         */
        $image = report_image::create([
            'report_id' => $report_id,
            'url_image' => $image_url,
        ]);
    }

}

?>