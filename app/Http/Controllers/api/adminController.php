<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\adminRepositoryInterface;

class adminController extends Controller
{
    /** Group of model as vars */
    protected $adminRepositoryInterface;

    /** Construct to handel inject models */
    public function __construct(adminRepositoryInterface $adminRepositoryInterface){
        $this->adminRepositoryInterface = $adminRepositoryInterface;
    }

    /** This Section To Add and Update Projects */
    /** Add Project */
    public function add_project(Request $request){
        return $this->adminRepositoryInterface->add_project($request);
    }

    /** Update Project */
    public function update_project(Request $request){
        return $this->adminRepositoryInterface->update_project($request);
    }


    /** This Section To Add and Update Zones */
    /** Add Zone */
    public function add_zone(Request $request){
        return $this->adminRepositoryInterface->add_zone($request);
    }

    /** Update Zone */
    public function update_zone(Request $request){
        return $this->adminRepositoryInterface->update_zone($request);
    }


    /** This Section To Add and Update Types and Sub_types */
    /** Add type */
    public function add_type(Request $request){
        return $this->adminRepositoryInterface->add_type($request);
    }

    /** Update type */
    public function update_type(Request $request){
        return $this->adminRepositoryInterface->update_type($request);
    }

}