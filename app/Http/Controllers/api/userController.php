<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\userRepositoryInterface;

class userController extends Controller
{
     /** Group of model as vars */
     protected $userRepositoryInterface;

     /** Construct to handel inject models */
     public function __construct(userRepositoryInterface $userRepositoryInterface){
         $this->userRepositoryInterface = $userRepositoryInterface;
     }

    /** User Projects */
    public function projects(){
        return $this->userRepositoryInterface->projects();
    }

    /** uploder Function */
    public function upload_files(Request $request){
        return $this->userRepositoryInterface->upload_files($request);
    }

    /** Add Report */
    public function add_report(Request $request){
        return $this->userRepositoryInterface->add_report($request);
    }

    /** Report Details */
    public function report_details($report_hash){
        return $this->userRepositoryInterface->report_details($report_hash);        
    }

    /** Reort List */
    public function reports_list(Request $request){
        return $this->userRepositoryInterface->reports_list($request);        
    }

    /** Add Report Note */
    public function add_note(Request $request){
        return $this->userRepositoryInterface->add_note($request);        
    }


    /** Dashboard Data */
    public function dashboard_data(Request $request){
        return $this->userRepositoryInterface->dashboard_data($request);        
    }


    /** All Notifcations */
    public function all_notifications(Request $request){
        return $this->userRepositoryInterface->all_notifications($request);        
    }

    /** Get Zone Images */
    public function get_zone_images(Request $request){
        return $this->userRepositoryInterface->get_zone_images($request);        
    }
 
}
