<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

use App\Http\Controllers\api\Traits\ApiResponseTrait;
use App\Http\Controllers\api\Traits\authTrait;
use App\Http\Controllers\api\Traits\tokenTrait;


use Mail;
use App\Mail\sendforgetpassword;

use App\User;

class AuthController extends Controller
{
    // Use Trair To Desgin API's.
    use ApiResponseTrait;
    use authTrait;
    use tokenTrait;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'logout', 'getAuthenticatedUser', 'forget_password', 'reset_password', 'create' , 'delete_user', 'update', 'update_password', 'me']]);
    }

   
    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        /**
         * Build:[
         *  request validation
         *  check if the user is active or not
         *  Login data
         * ]
        */
        $Validator = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422 , "Validation Errors", $Validator->errors());
        }

        $credentials = $request->only('email', 'password');

        $email_exist = $this->email_exist($request->email);
        if($email_exist == true){

            $check_user_active_status = $this->user_active_status($request->email);
            if($check_user_active_status == true){
                if ($token = auth()->attempt($credentials)) {
                    return $this->respondWithToken($token);
                }
                return $this->apiResponse(400 , 'Email or password are not correct');

            }else{
                return $this->apiResponse(400 , 'This User is not active');
            }
            
        }else{
            return $this->apiResponse(400 , 'Email or password are not correct');
        }

    }


    // Register Function for native user
    public function create(Request $request){
        /**
         * Build:[
         *  request validation
         *  Save user data into database
         *  save user projects with function add_projects_users
         * ]
        */
        $validator = validator::make($request->all() , [
            'email' => 'required|unique:users|email',
            'password' => 'required|min:8',
            'role' => [
                'required',
                Rule::In([0, 1]),
            ],
            'name' => 'required|min:3',
            'project_ids' => 'exists:projects,id', 

            
        ]);  
      

        if($validator->fails()){
            return $this->apiResponse(422 , "Validation Errors" , $validator->errors());
        }

            $user = User::create([
                'name' =>$request->name,
                'email' =>$request->email,
                'role' =>$request->role,
                'password' =>Hash::make($request->password),
            ]);

            if($request->project_ids){
                $this->add_projects_users($request->project_ids , $user->id);
            }
        
            return $this->apiResponse(200 , "Successfully Added", null, $user);
        
    }


    // Register Function for native user
    public function update(Request $request){
        /**
         * Build:[
         *  request validation
         *  update user data
         *  update user projects with function add_projects_users
         * ]
        */
        $validator = validator::make($request->all() , [
            'user_id' => 'exists:users,id', 
            'project_ids' => 'exists:projects,id', 
            'email' => [
                Rule::unique('users')->ignore($request->user_id),
            ],
            'password' => 'min:8',
            'role' => [
                Rule::In([0, 1]),
            ],
            'active' => [
                Rule::In([0, 1]),
            ],
            'name' => 'min:3',
        ]);  
        

        if($validator->fails()){
            return $this->apiResponse(422 , "Validation Errors" ,$validator->errors());
        }

            $user = User::select('id','name','email','role','active')->find($request->user_id);

            if($user){
                if($request->name){
                    $user->name = $request->name;
                }

                if($request->email){
                    $user->email = $request->email;
                }

                if(!is_null($request->role)){
                    $user->role = $request->role;
                }

                if(!is_null($request->active)){
                    $user->active = $request->active;
                }

                if($request->password){
                    $user->password = Hash::make($request->password);
                }
                $user->save();
            }

            if($request->project_ids){
                
                $this->add_projects_users($request->project_ids , $request->user_id);
            }

            return $this->apiResponse(200 , "updated", null, $user);
       
    }

    // Forget Password with Email
    public function forget_password(Request $request){
        /**
         * Build:[
         *  request validation.
         *  check if email is exist
         *  check if user active or not.
         *  genrate rand code.
         *  send code at email and save it at user record at database.
         * ]
         */

            $validator = validator::make($request->all() , [
                'email' => 'required|email',
            ]); 
            
            if($validator->fails()){
                return $this->apiResponse(422 , "Validation Errors" ,$validator->errors());
            }

            $email_exist = $this->email_exist($request->email);
            if($email_exist == true){

                $user_active_status = $this->user_active_status($request->email);
                if($user_active_status == true){

                $user = User::where('email', $request->email)->first();
                if($user){
                    $code = rand(10000,99999);
                    $user->update([
                        'rest_code' => $code
                    ]);
                    $data = [
                        "code" => $code,
                        "user_name" => $user->name ,
                        "user_email" => $user->email ,
                        "link" => env('ADMIN_URL')."/user/reset-password?code=".$code."&email=".$user->email
                    ];
                $mail = Mail::to($request->email)->send(new sendforgetpassword($data));

                    return $this->apiResponse(200 , "Password reset email is sent, Clink on the link and you will be redirected to Reset Password page!");
                }

            }else{
                return $this->apiResponse(422 , "This Account Not allowed to reset password");
            }
        }else{
            return $this->apiResponse(400 , "There is no account with this email");
        }
    }

    // Set New Password
    public function reset_password(Request $request){
        /**
         * Build:[
         *  request validation
         *  Update User Password and set rest_code as null.
         * ]
         */
        $validator = validator::make($request->all() , [
            'code' => 'required',
            'new_password' => 'required',
        ]); 
        
        if($validator->fails()){
            return $this->apiResponse(422 , "Validation Errors" ,$validator->errors());
        }

        $user = User::where('email', $request->email)->first();
        if($user && $user->rest_code == $request->code){
            $user->update([
                'password' => Hash::make($request->new_password),
                'rest_code' => null
            ]);
        }else{
            return $this->apiResponse(400 , "The action code is invalid or has already been used.");
        }

        return $this->apiResponse(200 , "Password reset completed, you will be redirected to Login page!");
        
    }


    // Update User Password
    public function update_password(Request $request){
        /**
         * Build:[
         *  Get User Data With Token.
         *  request validation.
         *  check if password matched with saved password.
         *  Update User Password and.
         *  logout other devices if it selected.
         * ]
         */
        $user_data = $this->getAuthenticatedUser();

        if($user_data){
            $validator = Validator::make($request->all(), [
                'password' => 'required',
                'new_password' => 'confirmed|max:8|different:password',
            ]);
            
                if(Hash::check($request->password, $user_data->password)){
                    $user_data->update([
                        "password" => Hash::make($request->new_password)
                    ]);

                    if($request->logout_other_devices = 1){
                        JWTAuth::invalidate(JWTAuth::getToken());
                    }
                    return $this->apiResponse(200 , "Password reset completed");
                }else{
                    return $this->apiResponse(422 , "Password is not correct");
                }

            }
        
    }


    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        $user = User::where('email' , $request->email)->first();
       
        if($user){
            $user_rate = rate_user::where('user_id', $user->id)->get();
            $total_rate = 0;
            foreach ($user_rate as $rate) {
                $total_rate += $rate->number;
            }
            $data = [
                'user' => $user,
                'user_rate' => $total_rate/2
            ];
            return $this->apiResponse($data , null , 200);
        }else{
            return $this->apiResponse(false , 'not found' , 404);
        }
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        /**
         * Build:[
         *  logout.
         * ]
         */
    
        Auth::guard('api')->logout();
        return $this->apiResponse(200 , "Logout completed!");
           
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }


     // User Data Function
     public function user_data($id , $token){
        $user = User::where("id" , $id)->first();

        $data = [
            'id' => $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
            'token' => $token,
            'role' => $user['role'],

        ];
        return $data;
    }


    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $data =  $this->user_data(auth()->user()->id , $token);

        return $this->apiResponse(200 , 'Successfully', null ,$data);

    }


    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('api');
    }
}
