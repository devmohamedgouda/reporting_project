<?php
namespace App\Http\Controllers\api;



trait ApiResponseTrait{

    /**
     * [
     *  data => 
     *  status =>
     *  code =>
     * ]
     */


     public function apiResponse($code = 200, $message = null, $data = null){

        $array = [
            'status' => $code,
            'message' => $message,
            'data' => $data,
        ];

        return response($array , 200);
     }

  


}

?>