<?php

namespace App\Http\Repositories;

use App\Http\Interfaces\adminRepositoryInterface;
use App\Http\Controllers\api\Traits\ApiResponseTrait;
use App\Http\Controllers\api\Traits\adminTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\project;
use App\zone;
use App\type;

class adminRepository implements adminRepositoryInterface{

    // Use Trair To Desgin API's.
    use ApiResponseTrait;
    use adminTrait;
    
    /** Group of model as vars */
    protected $project_model;
    protected $zone_model;
    protected $type_model;
    
    /** Construct to handel inject models */
    public function __construct(project $project, zone $zone, type $type){
        $this->project_model = $project;
        $this->zone_model = $zone;
        $this->type_model = $type;
    }

    /** This Section To Add and Update Projects */
    /** Add Project */
    public function add_project($request){

        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'name' => 'required|min:3',
            'description' => 'required',
            'lat' => 'required',
            'long' => 'required',
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }

        $project = $this->project_model::create([
            'name' => $request->name,
            'description' => $request->description,
            'lat' => $request->lat,
            'long' => $request->long,
            'active' => 1,
        ]);

        if($project){
            return $this->apiResponse(200, "Successfully Added", null, $project);
        }else{
            return $this->apiResponse(422, "unknown errors");
        }

    }

    /** Update Project */
    public function update_project($request){


        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'name' => 'required|min:3',
            'description' => 'required',
            'lat' => 'required',
            'long' => 'required',
            'project_id' => 'required|exists:projects,id', 
            'active' => [
                Rule::In([0, 1]),
            ],
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }

        $project = $this->project_model::find($request->project_id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'lat' => $request->lat,
            'long' => $request->long,
            'active' => $request->active,
        ]);
        $project_data = $this->project_model::select('id', 'name', 'description', 'lat', 'long', 'active')->find($request->project_id);

        if($project){
            return $this->apiResponse(200, "Successfully updated", null, $project_data);
        }else{
            return $this->apiResponse(422, "unknown errors");
        }

    }


    /** This Section To Add and Update Zones */
    /** Add Zone */
    public function add_zone($request){

        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'name' => 'required|min:3',
            'code' => 'required',
            'address' => 'required',
            'type' => 'required',
            'project_id' => 'required|exists:projects,id', 
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }

        // Check Project is active or not
        $check_project_status = $this->project_status($request->project_id);
        if($check_project_status == false){
            return $this->apiResponse(422, "Project is not active");
        }

        $zone = $this->zone_model::create([
            'name' => $request->name,
            'code' => $request->code,
            'address' => $request->address,
            'type' => $request->type,
            'project_id' => $request->project_id,
            'imgs_path' => null,
        ]);

        if($zone){
            return $this->apiResponse(200, "Successfully Added", null, $zone);
        }else{
            return $this->apiResponse(422, "unknown errors");
        }

    }

    /** Update Zone */
    public function update_zone($request){

        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'name' => 'required|min:3',
            'code' => 'required',
            'address' => 'required',
            'type' => 'required',
            'active' => [
                Rule::In([0, 1]),
            ],
            'project_id' => 'required|exists:projects,id', 
            'zone_id' => 'required|exists:zones,id', 
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }

        // Check Project is active or not
        $check_project_status = $this->project_status($request->project_id);
        if($check_project_status == false){
            return $this->apiResponse(422, "Project is not active");
        }

        $zone = $this->zone_model::find($request->zone_id);
        $zone->update([
            'name' => $request->name,
            'code' => $request->code,
            'address' => $request->address,
            'type' => $request->type,
            'active' => $request->active,
            'project_id' => $request->project_id,
        ]);
        $zone_data = $this->zone_model::select('id', 'name', 'code', 'address', 'type', 'project_id')->find($zone->id);

        if($zone){
            return $this->apiResponse(200, "Successfully updated", null, $zone_data);

        }else{
            return $this->apiResponse(422, "unknown errors");
        }

    }



    /** This Section To Add and Update Types and Sub_types */
    /** Add type */
    public function add_type($request){

        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'name' => 'required|min:3',
            'sub_types.*' => 'required|min:3',
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }

        $type = $this->type_model::create([
            'name' => $request->name,            
        ]);

        // To Add Sub Types
        if($request->sub_types){
          $this->add_sub_types_of_type($request->sub_types, $type->id);
        }


        if($type){
            $type_data = $this->get_types_and_sub_types($type->id);
            return $this->apiResponse(200, "Successfully Added", null, $type_data);
        }else{
            return $this->apiResponse(422, "unknown errors");
        }

    }
    

    /** Update type */
    public function update_type($request){
        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'name' => 'required|min:3',
            'sub_type' => 'required',
            'sub_type.*' => 'required|min:3',
            'type_id' => 'required|exists:types,id', 
            'active' => [
                Rule::In([0, 1]),
            ],

        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }

        $type = $this->type_model::find($request->type_id)->update([
            'name' => $request->name,            
            'active' => $request->active,            
        ]);

        // To Add Sub Types
        if($request->sub_type){
            $this->update_sub_type($request->sub_type, $request->type_id);
        }


        if($type){
            $type_data = $this->get_types_and_sub_types($request->type_id);
            return $this->apiResponse(200, "Successfully updated", null, $type_data);
        }else{
            return $this->apiResponse(422, "unknown errors");
        }

    }
        

}