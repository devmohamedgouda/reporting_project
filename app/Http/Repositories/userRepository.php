<?php

namespace App\Http\Repositories;

use App\Http\Interfaces\userRepositoryInterface;
use App\Http\Controllers\api\Traits\ApiResponseTrait;
use App\Http\Controllers\api\Traits\tokenTrait;
use App\Http\Controllers\api\Traits\adminTrait;
use App\Http\Controllers\api\Traits\uploaderTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Resources\projectsResource;
use App\Http\Resources\reportResource;
use App\Http\Resources\reportListResource;
use App\Http\Resources\NotificationsResourse;

use App\Notifications\ReportNotification;


use App\projects_user;
use App\type;
use App\report;
use App\report_type;
use App\report_note;
use App\project;
use App\zone;
use App\User;
use App\report_image;
use App\Services\StorageService;

class userRepository implements userRepositoryInterface{

    // Use Trair To Desgin API's.
    use ApiResponseTrait;
    use tokenTrait;
    use adminTrait;
    use uploaderTrait;
    
    /** Group of model as vars */
    protected $projects_user_model;
    protected $type_model;
    protected $report_model;
    protected $report_type_model;
    protected $report_note_model;
    protected $project_model;
    protected $zone_model;
    protected $users_model;
    protected $report_image_model;

    /** Construct to handel inject models */
    public function __construct(projects_user $projects_user, report_image $report_image, User $User, zone $zone, project $project, report_type $report_type, type $type, report $report, report_note $report_note){
        $this->projects_user_model = $projects_user;
        $this->type_model = $type;
        $this->report_model = $report;
        $this->report_type_model = $report_type;
        $this->report_note_model = $report_note;
        $this->project_model = $project;
        $this->zone_model = $zone;
        $this->users_model = $User;
        $this->report_image_model = $report_image;
    }

    /** User Projects */
    public function projects(){
        /**
         * Build:[
         *  Get User Data With Token.
         *  validate The Requrments.
         *  Select Project Data from projects_userwith project_data Relation.
         *  select User Data at anther Query and added it to projcts array.
         *  send the projects to projectsResource to Custmtize Return Data.
         *  Select All Types adn Sub Types and added it to array before return.
         * ]
         */
        $user_data = $this->getAuthenticatedUser();
        if($user_data){

            $projects = $this->projects_user_model::where('user_id', $user_data->id)
            ->whereHas('zones_data', function ($query) {
                $query->where('active','=','1');})

            ->whereHas('project_data', function ($query) {
                $query->where('active','=','1');})

                ->with(['zones_data:id,name,code,address,type'])->get();

            foreach ($projects as $project) {

                $assign_pepole = $this->projects_user_model::where('project_id', $project->project_id)->whereHas('user_data', function ($query) {
                    $query->where('role','=','1');
                })->with('user_data')->get();

                $project['users'] = $assign_pepole;                
            }
            
            $collection_projects = projectsResource::collection($projects);

            $projects_data = [
                "projects" => $collection_projects,
                "Report_types" => $this->get_types_data()
            ];

            return $this->apiResponse(200, "success", null, $projects_data);
        }
        
    }

    /** Add Report */
    public function add_report($request){

        /**
         * Build:[
         *  validate The Requrments.
         *  check if User has access or not
         *  upload main image.
         *  insert data at database.
         *  update hash_report with new id and time.
         *  uploaed images with upload_multi_files function
         *  Send Notifcation
         * ]
         */

        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'project_id' => 'required|exists:projects,id', 
            'zone_id' => 'required|exists:zones,id',
            'report_time' => 'required|max:17|regex:/(\d+\:\d+)/',
            'is_critical' => [
                'required',
                Rule::In([0, 1])
            ],
            'action_text' => 'required|min:3',
            'comment_text' => 'required|min:3',
            'main_image' => 'required',
            'is_report' => [
                'required',
                Rule::In([0, 1])
            ],
            'types' => 'required|exists:types,id', 
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }

            /** Check User If has access or not */
        $user_data = $this->getAuthenticatedUser();        
        $user_has_access = $this->user_has_access($user_data->id, $request->project_id);

        if($user_has_access == true){
            
            $report = $this->report_model::create([
                'project_id' => $request->project_id,
                'zone_id' => $request->zone_id,
                'report_time' => $request->report_time,
                'is_critical' => $request->is_critical,
                'action' => $request->action_text,
                'comment' => $request->comment_text,
                'main_image' => $request->main_image,
                'is_report' => $request->is_report,
                'user_id' => $user_data->id,
            ]);


            /** Uploaed Report Images */
            if($request->images){
                $images = explode(',',$request->images);
                foreach ($images as $image) {
                    $this->save_image_database($report->id, $image);
                }
            }

            /** Uploaed Report types */
            if($request->types){
                foreach ($request->types as $type) {
                    $this->add_type($type, $report->id);
                }
            }

            /** Set Hash Report */
            $report_hash = $this->report_model::find($report->id);
            $report_hash->update([
                'report_hash' => $report->id . time()
            ]);

            /** Send Notification */
            if($request->is_report == 1){
                $title = $user_data->name . " Send New Report";
                $type = 1;
            }else{
                $title = $user_data->name . " Send New Graph";
                $type = 2;
            }
            $data= [
                'title' => $title,
                'date'  => $report->created_at,
                'notification_type'  => $type,
                'report_hash'  => $report_hash->report_hash,
            ];
            
           $this->notifay_users($request->project_id, $data, $user_data->id);



            return $this->apiResponse(200, "success", null, $report_hash);
        }else{
            return $this->apiResponse(422, "User Don't Have Access");
        }
    }

    /** Report Details */
    public function report_details($report_hash){
        /**
         * Build:[
         *  Select Report Data.
         *  Select Report Images.
         *  Select Types and added it to report data.
         * ]
         */

        $data = $this->report_model::where('report_hash', $report_hash)->with('report_notes')->first();
        // Get Report images.
        if($data){
            $report_images = $this->report_image_model::where('report_id', $data->id)->get('url_image');
            foreach ($report_images as $image) {
                $totalReportImgs[] = $image->url_image;
            }
            $data['report_images'] = $totalReportImgs;

        
            $types = $this->get_types_data();
            $data['types'] = $types;
            $collection_report = new reportResource($data);
            return $this->apiResponse(200, "success", null, $collection_report);
            
        }else{
            return $this->apiResponse(422, "not found");
        }

    }

    /** Reort List */
    public function reports_list($request){
      
        /**
         * Build:[
         *  Seelct Data With Realtions.
         *  Collection Data.
         * ]
         */
        $user_data = $this->getAuthenticatedUser();
        if($user_data){

            $pagesize = 20;
            if($request->pagesize) $pagesize = $request->pagesize;
            

            $query = $this->report_model::skip($request->page * $request->pagesize - $request->pagesize)->where('user_id', $user_data->id)->whereHas("project", function ($q) {
                return $q->where("active", 1);
            })->whereHas("zone", function ($q) {
                return $q->where("active", 1);
            })
            ->select('id', 'report_hash', 'project_id', 'zone_id', 'report_time', 'is_critical', 'comment', 'main_image', 'created_at');
        
            $total_reports = $this->report_model::where('user_id', $user_data->id)->whereHas("project", function ($q) {
                return $q->where("active", 1);
            })->whereHas("zone", function ($q) {
                return $q->where("active", 1);
            })->count();

            $total_pages = ceil($total_reports / $pagesize);
    
            if ($request->projcts_ids) {
                $query->whereIn("project_id", $request->project_ids);
            }
    
            if ($request->zone_ids) {
                $query->whereIn("zone_id", $request->zone_ids);
            }
    
            if ($request->is_report) {
                $query->where("is_report", $request->is_report);
            }
    
            if ($request->is_critical) {
                $query->where("is_critical", $request->is_critical);
            }
    
            if ($request->date_from) {
                $query->where("created_at", ">=", $request->date_from);
            }
    
            if ($request->date_to) {
                $query->where("created_at", "<=", $request->date_to);
            }
    
            $data = $query->limit($pagesize)->get();
    
            // $user_report_types= [];
            foreach ($data as $report) {
                $report_types = $this->report_type_model::where('report_id', $report->id)->get(['id']);
                foreach ($report_types as $type) {
                    $user_report_types[] =$type->id;
                }


                $report['types'] = $user_report_types;
                // dd($user_report_types);
                unset($user_report_types);
                $user_report_types = array();
                
            }
         
            $total_data=[
                "reports" => $data,
                "total_pages" => $total_pages
            ];
    
            return $this->apiResponse(200, "Success", null, $total_data);
    
        }else{
            return $this->apiResponse(422, "Unknown error");
        }

    }

    /** Add Report Note */
    public function add_note($request){
        /**
         * Build:[
         *  validate The Requrments.
         *  check if User has access or not. 
         *  insert data at database.
         * ]
         */

        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'report_id' => 'required|exists:reports,id', 
            'comment' => 'required|min:3'
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }  

        $user_data = $this->getAuthenticatedUser();
        if($user_data){

            /** Check User If has access or not */
            $project_id = $this->get_project_id($request->report_id);
            $user_has_access = $this->user_has_access($user_data->id, $project_id);

            if($user_has_access == true){
                $note = $this->report_note_model::create([
                    'report_id' => $request->report_id,
                    'user_id' => $user_data->id,
                    'comment' => $request->comment,
                ]);

                /** Send Notification */
                $project_id = $this->get_project_id($request->report_id);
                $report_hash = $this->report_model::where('id', $request->report_id)->first('report_hash');
                $data= [
                    'title' => $user_data->name . " Send New Note",
                    'date'  => $note->created_at,
                    'notification_type'  => 3,
                    'report_hash'  => $report_hash->report_hash,
                ];
               $this->notifay_users($project_id, $data, $user_data->id);
    

                return $this->apiResponse(200, "Successfully created");
            }else{
                return $this->apiResponse(422, "User Don't Have Access");

            }
       

        }else{
            return $this->apiResponse(422, "unknown error");
        }

    }

    /** Dashboard Data */
    public function dashboard_data($request){
        /**
         * Build:[
         *  Seelct Data With Realtions.
         *  Collection Data.
         * ]
         */

         /** Validate The Requrments */
         $Validator = Validator::make($request->all(),[
            'project_ids' => 'exists:projects,id', 
            'zone_ids' => 'exists:zones,id', 
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }  


        $user_data = $this->getAuthenticatedUser();
        if($user_data){
            $total_projects = $this->project_model::count();
            $total_zones = $this->zone_model::count();
            $total_reports = $this->report_model::where('is_report', 0)->count();
            $total_graphs = $this->report_model::where('is_report', 1)->count();

            /** Get Report  Of Sammry */

            // Reports Count Per Project.
            $projects_report_count = $this->project_model::select('id', 'name')->withCount('reports_project')->get();
            foreach ($projects_report_count as $project) {
                $projects_report_count_data = [
                    'project_name' => $project->name,
                    'count' => $project->reports_project_count
                ];
            }


            // Reports Count Per Types.           
            $types_report_count = $this->type_model::select('id', 'name')->where('parent', null)->withCount('type_reports')->get();
            foreach ($types_report_count as $type) {
                $types_report_count_data['labels'] = [
                     $type->name
                ];

                $types_report_count_data['data'] = [
                    $type->type_reports_count
               ];
               
            }


            // Reports Count Per Sub Types.           
            $sub_types_report_count = $this->type_model::select('id', 'name')->where('parent', '!=' , null)->withCount('type_reports')->get();
            foreach ($sub_types_report_count as $sub_type) {
                $sub_types_report_count_data = [
                    'subtype_name' => $sub_type->name,
                    'count' => $sub_type->type_reports_count
                ];
            }


    
            $projects_zones_report_type_count = $this->zone_model::where('active', 1)->get(['id','name']);
            
            
            foreach ($projects_zones_report_type_count as $zone) {
                $projects_zones_report_type_count_data['labels'] = $zone->name;

               
                $data = $this->type_model::where('active', 1)
                ->whereHas("type_reports.report", function($q) use($zone){
                    return $q->where('zone_id',$zone->id);
                })
                ->get();
                $projects_zones_report_type_count_data['label']= [];
                foreach ($data as $value) {
                    $projects_zones_report_type_count_data['label'] = $value->name;
                    // $projects_zones_report_type_count_data["dataset"] =[
                    //     "label" => $value
                    // ];
                }

                // for ($i=0; $i < $data->count(); $i++) { 
                //     $projects_zones_report_type_count_data["datasets"]=[
                //         "label" => $data[$i]->name
                //     ]; 
                // }
          

               

              
              
            }
           
            return $projects_zones_report_type_count_data;

            $recent_reports = $this->report_model::where('user_id', $user_data->id)->orderBy('id', 'DESC')->limit(10)->get(['id','comment','created_at','main_image']);
            foreach ($recent_reports as $report) {

                $report_types = $this->report_type_model::where('report_id', $report->id)->with('type')->get();
                foreach ($report_types as $type) {
                    $user_report_types[] = [$type->id , $type->type->name];
                }
                $report['types'] = $user_report_types;
            }

            $data = [
                "total_projects" => $total_projects,
                "total_zones" => $total_zones,
                "total_reports" => $total_reports,
                "total_graphs" => $total_graphs,

                "recent_reports" => $recent_reports,
                
                "projects_report_count" => $projects_report_count_data,
                "types_zones_count" => $types_zones_count,
                "types_report_count" => $types_report_count_data,
                "sub_types_report_count" => $sub_types_report_count_data,
            ];

            return $this->apiResponse(200, "Success", $data);
        }
    }

    /** All Notifcations */
    public function all_notifications($request){
         /**
         * Build:[
         *  Get User Data with token.
         *  Seelct notifications With paginate.
         *  mark as read if user needed.
         * ]
         */
        $user_data = $this->getAuthenticatedUser();

        $user = $this->users_model::find($user_data->id);
        $data = $user->notifications()->select('data', 'id', 'read_at')->skip($request->page * 10 - 10)->limit(10)->get();
        $unread = $user->notifications()->where('read_at', null)->select('read_at')->count();


        if($request->read == 1){
           $this->mark_as_read_notification($data);
        }

        // Get Total Pages...
        $total_pages = round($user->notifications()->count() /10) == 0 ? 1 : round($user->notifications()->count() /10);

        // $data['page'] = $total_page;
        $notification = NotificationsResourse::collection($data);
        $total_data = [
            "notifications" => $notification,
            "total_unread" => $unread,
            "total_pages" => $total_pages,
        ];

        return $this->apiResponse(200, "Success", null, $total_data );

    }

    /** Get Zone Images */
    public function get_zone_images($request){

        $storageService = new StorageService;

        $zone_images = $storageService->getFolderImages("project1", "zone1", $request->date);

        return $this->apiResponse(200, "Success", $zone_images);


    }
        
   
    
    /** uploder Function */
    public function upload_files($request){
        /**
         * Build:[
         *  validate The Requrments.
         *  upload files with upload_multi_files function.
         * ]
        */

        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'files' => 'required', 
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }  

        $files = $this->upload_multi_files($request->file('files'));
        return $this->apiResponse(200, "Success", $files);
    }

        

}